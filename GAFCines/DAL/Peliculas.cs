//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Peliculas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Peliculas()
        {
            this.Funciones = new HashSet<Funciones>();
        }
    
        public int id { get; set; }
        public int codigoPelicula { get; set; }
        public string pref { get; set; }
        public string nombre { get; set; }
        public string abreviatura { get; set; }
        public string descripcion { get; set; }
        public string datosTecnicos { get; set; }
        public string urlTrailer { get; set; }
        public bool vender { get; set; }
        public bool mostrar { get; set; }
        public int idCine { get; set; }
    
        public virtual Cines Cines { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Funciones> Funciones { get; set; }
    }
}
