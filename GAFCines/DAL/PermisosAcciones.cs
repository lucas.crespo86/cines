//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class PermisosAcciones
    {
        public int id { get; set; }
        public int idAccion { get; set; }
        public int idPermiso { get; set; }
    
        public virtual Acciones Acciones { get; set; }
        public virtual Permisos Permisos { get; set; }
    }
}
