//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ventas
    {
        public int id { get; set; }
        public int cantidad { get; set; }
        public decimal importe { get; set; }
        public System.DateTime fechaVenta { get; set; }
        public int codigoVenta { get; set; }
        public string detalle { get; set; }
        public int codigoTipo { get; set; }
        public string email { get; set; }
        public string nroDocumento { get; set; }
        public int idPelicula { get; set; }
        public int idCine { get; set; }
        public int idEmpresa { get; set; }
        public Nullable<int> idFuncionDetalle { get; set; }
        public string nombrePelicula { get; set; }
        public System.DateTime fechaHora { get; set; }
        public string fechaString { get; set; }
        public string horaString { get; set; }
        public string lenguaje { get; set; }
        public string formato { get; set; }
        public string sala { get; set; }
        public int codigoFuncionDetalle { get; set; }
        public string dondeCompro { get; set; }
        public string dondeRetiro { get; set; }
        public string estado { get; set; }
        public string nroEntrada { get; set; }
        public int posicion { get; set; }
    }
}
