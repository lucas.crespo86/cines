﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Funcion.aspx.cs" Inherits="GAFCines.Funcion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>

    <section class="container" style="min-height: 900px">
        <div class="order-container">
            <div class="order">
                <img class="order__images" alt="" src="assets/images/tickets.png">
                <p class="order__title">
                    Compra tu entrada
                    <br>
                    <span class="order__descript">y disfrutá de tu película favorita</span>
                </p>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-12">
                <%DAL.Funciones funcion = new DAL.Funciones();
                    string cref = Request.QueryString["cref"];
                    string fref = Request.QueryString["fref"];
                    funcion = BLL.Funciones.TraerUno(fref, cref);
                    %>
                <div class="movie">
                    <h1 class="page-heading"><%= funcion.Peliculas.nombre%></h1>
                    <h5 style="margin-top: 20px">Cine: <b><%= funcion.Cines.nombre %></b>
                        <br />
                        <br />
                        Día: <b><%= BLL.Common.DiaDeSemana(funcion.fechaHoraFicticia) + " " + funcion.fechaHoraFicticia.Date.Day + " de " + BLL.Common.NombreDelMes(funcion.fechaHoraFicticia) %></b>
                        <br />
                        <br />
                        Función: <b><%= funcion.fechaHoraFicticia.ToString("HH:mm")%> hs </b>| Sala: <b><%= funcion.sala %></b> | Lenguaje: <b><%= funcion.lenguaje %></b>
                    </h5>
                    <div class="clearfix"></div>
                    <div class="movie__info">
                        <%if (BLL.Cines.TraerUno(cref).servidorPropio == true)
                        {%>
                        <div class="col-sm-4 col-md-3 movie-mobile">
                            <div class="movie__images">
                                <img alt="" src="assets/images/peliculas/<%= funcion.Peliculas.codigoPelicula%>.jpg">
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-9" style="padding-top: 20px">
                        <%}
                        else
                        {%>
                            <div class="col-sm-12 col-md-12" style="padding-top: 20px">
                        <% }%>
                             <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="update">
                                <ProgressTemplate>
                                    <div style="height: 100%; background-color: White; opacity: 0.75; filter: alpha(opacity=75); vertical-align: middle; left: 0px; z-index: 999999; width: 100%; position: absolute; text-align: center;">
                                        <br />
                                        <img src="assets/images/ajax-loader.GIF" />
                                        <p>Procesando...</p>
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel ID="update" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>
                                    <div id="pnlEntrada" runat="server" class="step">
                                        <div class="order-step-area">
                                            <div class="order-step first--step">1.ENTRADAS</div>
                                            <div class="order-step third--step order-step--disable">2. PAGAR</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h6 class="page-header">SELECCIONE CANTIDAD DE ENTRADAS</h6>
                                                <div class="table-responsive">
                                                    <asp:GridView ID="gvEntradas" runat="server"
                                                        BorderColor="#ffffff" HeaderStyle-BorderColor="#ffffff"
                                                        HeaderStyle-BackColor="#ffd564" HeaderStyle-Height="50px" RowStyle-Height="40px"
                                                        ShowFooter="true" EnableModelValidation="True" AutoGenerateColumns="False"
                                                        OnRowCommand="gvEntradas_RowCommand" OnRowDataBound="gvEntradas_RowDataBound">
                                                        <Columns>
                                                            <asp:BoundField DataField="Detalle" HeaderText="Tipo de Entrada" />
                                                            <asp:TemplateField HeaderText="Cantidad">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnRestar" runat="server" CssClass="btn btn-xs btn-danger" CommandName="Resta" CommandArgument='<%# Container.DataItemIndex %>' UseSubmitBehavior="false">-</asp:LinkButton>
                                                                    <asp:Label ID="lblCantidad" runat="server" Text='<%# Bind("cantidad") %>' />
                                                                    <asp:LinkButton ID="btnSumar" runat="server" CssClass="btn btn-xs btn-success" CommandName="Suma" CommandArgument='<%# Container.DataItemIndex %>' UseSubmitBehavior="false">+</asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Precio" ItemStyle-Width="160px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblImporteUnitario" runat="server" Text='<%# Bind("importeUnitarioString") %>' />
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:Label ID="lblImporteUnitarioTotal" runat="server" Text="TOTAL" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Total" ItemStyle-Width="160px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblImporte" runat="server" Text='<%# Bind("importeString") %>' />
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:Label ID="lblImporteTotal" runat="server" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <FooterStyle Height="80px" CssClass="active text-center text-success h4" Font-Bold="true" />
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group text-right">
                                                    <asp:Button ID="btnSiguienteEntradas" runat="server" CssClass="btn btn-md btn--warning btn--wide" Text="Siguiente" OnClick="btnSiguienteEntradas_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Step -->

                                    <!-- paso 4 -->
                                    <div id="pnlPagar" runat="server" class="step">
                                        <div class="order-step-area">
                                            <div class="order-step first--step order-step--disable ">1. ENTRADAS</div>
                                            <div class="order-step third--step">2. PAGAR</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h6 class="page-header">DETALLE DE COMPRA</h6>
                                                <asp:GridView ID="gvPagar" runat="server"
                                                    BorderColor="#ffffff" HeaderStyle-BorderColor="#ffffff"
                                                    HeaderStyle-BackColor="#f5f5f5" HeaderStyle-Height="50px" RowStyle-Height="40px"
                                                    ShowFooter="true" EnableModelValidation="True" AutoGenerateColumns="False" OnRowDataBound="gvPagar_RowDataBound">
                                                    <Columns>
                                                        <asp:BoundField DataField="Detalle" HeaderText="Tipo de Entrada" />
                                                        <asp:TemplateField HeaderText="Cantidad">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCantidad" runat="server" Text='<%# Bind("Cantidad") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Precio" ItemStyle-Width="160px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblImporteUnitario" runat="server" Text='<%# Bind("ImporteUnitarioString") %>' />
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="lblImporteUnitarioTotal_Pagar" runat="server" Text="TOTAL" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Total" ItemStyle-Width="160px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblImporte_Pagar" runat="server" Text='<%# Bind("ImporteString") %>' />
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="lblImporteTotal_Pagar" runat="server" />
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <FooterStyle Height="80px" CssClass="active text-center text-success h4" Font-Bold="true" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group text-right">
                                                    <asp:Button ID="btnVolverPagar" runat="server" Text="Anterior" CssClass="btn btn-md btn--warning btn--wide" OnClick="btnVolverPagar_Click" />
                                                    <asp:Button ID="btnPagar" runat="server" CssClass="btn btn-md btn--success btn--wide" Text="PAGAR" Width="150px" OnClick="btnPagar_Click" /><br />
                                                    <asp:Label ID="lblMensaje" CssClass="alert-danger" Visible="false" runat="server">Mensaje</asp:Label>
                                                </div>
                                                <br />
                                                <div class="form-group text-right">
                                                    <img src="https://imgmp.mlstatic.com/org-img/banners/ar/medios/125X125.jpg" title="MercadoPago - Medios de pago" alt="MercadoPago - Medios de pago" width="125" height="125" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="gvEntradas" />
                                    <asp:AsyncPostBackTrigger ControlID="btnPagar" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>


                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
</asp:Content>
