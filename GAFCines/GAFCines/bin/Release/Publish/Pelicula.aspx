﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Pelicula.aspx.cs" Inherits="GAFCines.Pelicula" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" runat="server">

    <section class="container">

        <div class="col-sm-12">
            <%DAL.Peliculas pelicula = new DAL.Peliculas();
                string cref = Request.QueryString["cref"];
                string pref = Request.QueryString["pref"];
                pelicula = BLL.Peliculas.TraerUno(pref, cref);%>
            <div class="movie">
                <h2 class="page-heading"><%= pelicula.nombre%></h2>
                <h5 style="margin-top: 20px">CINE: <b><%= pelicula.Cines.nombre %></b>
                </h5>
                <div class="movie__info">
                    <% DAL.Cines cine = BLL.Cines.TraerUno(cref);
                        if (cine.servidorPropio == true)
                        {%>
                    <div class="col-sm-4 col-md-3 movie-mobile">
                        <div class="movie__images">
                            <img alt="" src="assets/images/peliculas/<%= pelicula.codigoPelicula%>.jpg">
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-9">
                        <%}
                            else
                            {%>
                        <div class="col-sm-12 col-md-12" style="padding-top: 20px">
                            <% }%>
                            <p class="movie__option"><strong>Datos Técnicos: </strong><%= pelicula.datosTecnicos%></p>
                            <p class="movie__option"><strong>Descripción: </strong><%= pelicula.descripcion%></p>
                            <%if (cine.servidorPropio == true && pelicula.urlTrailer != null)
                                {%>
                            <style>
                                .video-container {
                                    position: relative;
                                    padding-bottom: 56.25%;
                                    padding-top: 30px;
                                    height: 0;
                                    overflow: hidden;
                                }

                                    .video-container iframe,
                                    .video-container object,
                                    .video-container embed {
                                        position: absolute;
                                        top: 0;
                                        left: 0;
                                        width: 100%;
                                        height: 100%;
                                    }
                            </style>
                            <h2 class="page-heading">Trailer</h2>
                            <div class="video-container">
                                <iframe src="<%= pelicula.urlTrailer %>" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <%}; %>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="col-sm-12">
                <h2 class="page-heading">Funciones</h2>
                <div class="choose-container">
                    <div class="time-select">
                        <%List<DAL.Funciones> funciones = BLL.Funciones.TraerTodos(pelicula);
                            if (funciones.Count == 0)
                            {%>
                        <div class="time-select__group">
                            <div class="col-sm-12">
                                <p class="time-select__place">No hay funciones disponibles a la compra por este medio</p>
                            </div>
                        </div>
                        <%}
                            List<DateTime> fechas = funciones.Select(x => x.fechaHoraFicticia.Date).Distinct().ToList();
                            List<DAL.Funciones> funcionesPorFecha;
                            foreach (var item in fechas)
                            {
                                funcionesPorFecha = funciones.Where(x => x.fechaHoraFicticia.Date == item.Date).OrderBy(x => x.fechaHora).ToList();%>
                        <div class="time-select__group group--first">
                            <div class="col-sm-4">
                                <p class="time-select__place"><%= BLL.Common.DiaDeSemana(item) + " " + item.Date.Day + " de " + BLL.Common.NombreDelMes(item) %></p>
                            </div>
                            <div class="col-sm-8 items-wrap">
                                <% foreach (var f in funcionesPorFecha)
                                    {
                                        if (f.vender == true)
                                        {%>
                                <a class="time-select__item" href="funcion.aspx?fref=<%= f.fref%>&cref=<%= f.Cines.cref%>"><%= f.fechaHoraFicticia.ToString("HH:mm")%>hs | <%=f.lenguaje %></a>
                                <% }

                                    else
                                    {%>
                                    <div class="time-select__item" style="background-color:#d4d4d4">
                                    <%= f.fechaHoraFicticia.ToString("HH:mm")%>hs | <%=f.lenguaje %>
                                </div>
                                <% }%>

                                <%}%>
                            </div>
                        </div>
                        <%}%>
                    </div>
                </div>
            </div>
    </section>
</asp:Content>
