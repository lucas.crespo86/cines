﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="GAFCines.Index" %>

<asp:Content ContentPlaceHolderID="cphMain" runat="server">
        <!-- Slider -->
        <div class="bannercontainer rev_slider_wrapper">
            <img class="rev-slidebg" alt='' src="images/banners/index.jpg">
        </div>
        <!-- END SLIDER CONTAINER WRAPPER -->

        <section class="container" style="min-height:500px">
            <div class="col-sm-12">
                <h2 class="page-heading">PELÍCULAS EN CARTELERA</h2>
                <div class="select-area" style="height: 60px">
                    <span class="datepicker__marker">SELECCIONE CINE</span>
                    <asp:DropDownList ID="ddlCines" runat="server" AutoPostBack="true" DataSourceID="ObjectDataSource1" DataTextField="nombre" DataValueField="cref" Style="min-width: 100px; height: 30px"></asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="TraerTodos" TypeName="BLL.Cines"></asp:ObjectDataSource>
                </div>
                <%foreach (DAL.Peliculas pelicula in BLL.Peliculas.TraerTodosConFuncion(ddlCines.SelectedValue))
                    {
                %>
                <div class="movie movie--preview movie--full release">

                    <div class="col-sm-3 col-md-2 col-lg-2">
                            <div class="movie__images">
                                <img alt="" src="assets/images/peliculas/<%= pelicula.codigoPelicula%>.jpg">
                            </div>
                    </div>
                    <div class="col-sm-9 col-md-10 col-lg-10 movie__about">

                        <label class="movie__title link--huge""><%= pelicula.nombre%></label>
                        <hr>
                        <%= BLL.Funciones.TraerHorarios(pelicula) %>
                    </div>
                </div>
                <hr />
                <div class="clearfix"></div>
                <%} %>
            </div>
            <!-- end movie preview item -->
            <%--<aside class="col-sm-3">
                <div class="sitebar first-banner--left">
                    <div class="banner-wrap first-banner--left">
                        <img alt="banner" src="assets/images/banners/1.jpg">
                    </div>
                    <div class="banner-wrap first-banner--left">
                        <img alt="banner" src="assets/images/banners/2.jpg">
                    </div>
    
                </div>
            </aside>--%>
        </section>
        <section class="container">
            <div class="clearfix"></div>
            
        </section>
   
</asp:Content>
