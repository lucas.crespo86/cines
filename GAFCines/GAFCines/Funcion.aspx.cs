﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using MercadoPago.Common;
using MercadoPago.DataStructures.Preference;
using MercadoPago.Resources;
using System.Collections;

namespace GAFCines
{
    public partial class Funcion : System.Web.UI.Page
    {
        public virtual List<BLL.VentasDTO> ventas
        {
            get
            {
                object o = ViewState["ventas"];
                return (o == null) ? null : (List<BLL.VentasDTO>)o;
            }

            set
            {
                ViewState["ventas"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string cref = Request.QueryString["cref"];
                string fref = Request.QueryString["fref"];


                DAL.Funciones funcion = BLL.Funciones.TraerUno(fref, cref);

                ventas = new List<BLL.VentasDTO>();
                BLL.VentasDTO venta;
                foreach (DAL.FuncionesDetalle item in funcion.FuncionesDetalle)
                {
                    venta = new BLL.VentasDTO();
                    venta.cantidad = 0;
                    venta.idCine = funcion.idCine;
                    venta.codigoTipo = item.codigoTipo;
                    venta.detalle = item.detalle;
                    venta.idFuncionDetalle = item.id;
                    venta.fechaHora = funcion.fechaHora;
                    venta.formato = funcion.formato;
                    venta.idEmpresa = funcion.Cines.idEmpresa;
                    venta.importe = item.precio * venta.cantidad;
                    venta.lenguaje = funcion.lenguaje;
                    venta.idPelicula = funcion.idPelicula;
                    venta.sala = funcion.sala;
                    venta.codigoFuncionDetalle = item.codigoFuncionDetalle;
                    venta.estado = "Reservada";
                    venta.dondeCompro = "Web";
                    venta.nroEntrada = item.nroEntrada;

                    //DTO
                    venta.importeUnitarioString = item.precio.ToString("N2");
                    venta.importeUnitario = item.precio;
                    venta.importeString = venta.importe.ToString("N2");

                    ventas.Add(venta);
                }

                gvEntradas.DataSource = ventas;
                gvEntradas.DataBind();

                pnlEntrada.Visible = true;
                pnlPagar.Visible = false;
                update.Update();
            }

        }

        //paso 3 pnlEntrada
        protected void btnSiguienteEntradas_Click(object sender, EventArgs e)
        {
            gvPagar.DataSource = ventas.Where(x => x.cantidad > 0).ToList();
            gvPagar.DataBind();

            pnlPagar.Visible = true;
            pnlEntrada.Visible = false;
            update.Update();
        }


        decimal total = 0M;
        protected void gvEntradas_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblImporte = (Label)e.Row.FindControl("lblImporte");

                decimal importe = Decimal.Parse(lblImporte.Text);

                total += importe;
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblImporteTotal = (Label)e.Row.FindControl("lblImporteTotal");

                lblImporteTotal.Text = "$ " + total.ToString("N2");
            }

            btnSiguienteEntradas.Visible = false;
            if (total > 0)
            {
                btnSiguienteEntradas.Visible = true;
            }
        }

        protected void gvEntradas_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Suma")
            {
                int idex = Convert.ToInt32(e.CommandArgument);
                BLL.VentasDTO venta = ventas.ElementAt(idex);
                if (venta.cantidad < 9)
                {
                    venta.cantidad = venta.cantidad + 1;
                }
                venta.importe = venta.cantidad * venta.importeUnitario;
                venta.importeString = (venta.cantidad * venta.importeUnitario).ToString("N2");
                gvEntradas.DataSource = ventas;
                gvEntradas.DataBind();
                update.Update();
            }
            if (e.CommandName == "Resta")
            {
                int idex = Convert.ToInt32(e.CommandArgument);
                BLL.VentasDTO venta = ventas.ElementAt(idex);
                if (venta.cantidad > 0)
                {
                    venta.cantidad = venta.cantidad - 1;
                }
                venta.importe = venta.cantidad * venta.importeUnitario;
                venta.importeString = (venta.cantidad * venta.importeUnitario).ToString("N2");
                gvEntradas.DataSource = ventas;
                gvEntradas.DataBind();
                update.Update();
            }
        }

        //Paso 4 pnlPagar
        decimal totalPagar = 0M;
        protected void gvPagar_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblImporte = (Label)e.Row.FindControl("lblImporte_Pagar");

                decimal importe = Decimal.Parse(lblImporte.Text);

                totalPagar += importe;
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblImporteTotal = (Label)e.Row.FindControl("lblImporteTotal_Pagar");

                lblImporteTotal.Text = "$ " + totalPagar.ToString("N2");
            }
        }

        protected void btnVolverPagar_Click(object sender, EventArgs e)
        {
            pnlPagar.Visible = false;
            pnlEntrada.Visible = true;
            update.Update();
        }

        protected void btnPagar_Click(object sender, EventArgs e)
        {
            string cref = Request.QueryString["cref"];
            string fref = Request.QueryString["fref"];

            DAL.Funciones funcion = BLL.Funciones.TraerUno(fref, cref);

            lblMensaje.Visible = false;
            bool vendio = true;

            Random random = new Random();
            int codigo = random.Next(10000000, 99999999);
            decimal total = 0;
            DateTime fecha = DateTime.Now;
            DateTime fechaExpiracion = DateTime.Now.AddMinutes(10);

            int cantidadVentaTotal = 0;
            foreach (BLL.VentasDTO item in ventas)
            {
                if (item.cantidad > 0)
                {
                    item.nombrePelicula = funcion.Peliculas.nombre;
                    item.fechaString = BLL.Common.DiaDeSemana(funcion.fechaHoraFicticia) + " " + funcion.fechaHoraFicticia.Date.Day + " de " + BLL.Common.NombreDelMes(funcion.fechaHoraFicticia);
                    item.horaString = funcion.fechaHoraFicticia.ToString("HH:mm");
                    item.codigoVenta = codigo;
                    item.fechaVenta = fecha;
                    //item.id = BLL.Ventas.Create(item).id;

                    total += item.importe;
                    cantidadVentaTotal = cantidadVentaTotal + item.cantidad;
                }
            }

            if (funcion.vender == true)
            {
                int cantidadVendidas = BLL.Funciones.TraerCantidadVendidas(funcion);
                cantidadVendidas = cantidadVendidas + cantidadVentaTotal;
                DateTime ahora = DateTime.Now.AddMinutes(funcion.Cines.minutosHabilitados);
                if ((cantidadVendidas <= funcion.maxVentas || funcion.maxVentas == 0)
                    && funcion.fechaHoraFicticia > ahora)
                {
                    try
                    {
                        MercadoPago.SDK.CleanConfiguration();
                        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        MercadoPago.SDK.ClientId = funcion.Cines.Empresas.mpClientId;
                        MercadoPago.SDK.ClientSecret = funcion.Cines.Empresas.mpClientSecret;

                        string externalReference = funcion.Cines.abreviatura + "_" + codigo.ToString();

                        string success = funcion.Cines.paginaWebNotificaion + "success.html";
                        string pending =  funcion.Cines.paginaWebNotificaion + "pending.html";
                        string failure = funcion.Cines.paginaWebNotificaion + "failure.html";

                        DAL.Cines cine = BLL.Cines.TraerUno(cref);

                        // Create a preference object
                        var preference = new Preference
                        {

                            Items =
                            {
                                new Item
                                {
                                    Id = "1",
                                    Title = "COMPRA DE ENTRADA " + funcion.Cines.nombre,
                                    Description = "PELICULA: "+ funcion.Peliculas.nombre.Trim() +", FECHA: " + ventas[0].fechaString + ", HORA: " + ventas[0].horaString +", CANTIDAD ENTRADAS: " + cantidadVentaTotal,
                                    Quantity = 1,
                                    CurrencyId = CurrencyId.ARS,
                                    UnitPrice = total,
                                }
                            },
                            BinaryMode = true,
                            AutoReturn = AutoReturnType.approved,
                            PaymentMethods = new PaymentMethods
                            {
                                ExcludedPaymentTypes = new List<PaymentType> 
                                {
                                    new PaymentType
                                    {
                                        Id = PaymentTypeId.ticket.ToString()
                                    },
                                    new PaymentType
                                    {
                                        Id = PaymentTypeId.prepaid_card.ToString()
                                    },
                                    new PaymentType
                                    {
                                        Id = PaymentTypeId.bank_transfer.ToString()
                                    },
                                    new PaymentType
                                    {
                                        Id = PaymentTypeId.atm.ToString()
                                    }

                                },
                                Installments = 1,
                                DefaultInstallments = null,
                                DefaultPaymentMethodId = null,
                            },
                            NotificationUrl = cine.Empresas.ipn,
                            ExternalReference = externalReference,
                            Datecreated = fecha,
                            ExpirationDateFrom = fecha,
                            ExpirationDateTo = fechaExpiracion,
                            Expires = true,
                            BackUrls = new BackUrls
                            {
                                Success = success,
                                Failure = failure,
                                Pending = pending,
                            }
                        };
                        preference.Save();

                        foreach (BLL.VentasDTO item in ventas)
                        {
                            if (item.cantidad > 0)
                            {
                                item.id = BLL.Ventas.Create(item).id;
                            }
                        }

                        Response.Redirect(preference.InitPoint, false);
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Visible = true;
                        lblMensaje.Text = ex.Message;
                    }
                }
                else
                {
                    vendio = false;
                    BLL.Funciones.CambiarEstado(funcion.id, false);
                }
            }
            else
            {
                vendio = false;
            }

            if (vendio == false)
            {
                lblMensaje.Visible = true;
                lblMensaje.Text = "No hay más entradas disponibles para la función por este medio";
            }

            update.Update();
        }

        //Paso 5 pnlPagado
        protected void btnVolverPagado_Click(object sender, EventArgs e)
        {
            //string fref = Request.QueryString["fref"];
            //string cref = Request.QueryString["cref"];

            //DAL.Funciones funcion = BLL.Funciones.TraerUno(fref, cref);

            //ventas = new List<BLL.VentasDTO>();
            //BLL.VentasDTO venta;
            //foreach (DAL.FuncionesDetalle item in funcion.FuncionesDetalle)
            //{
            //    venta = new BLL.VentasDTO();
            //    venta.cantidad = 1;
            //    venta.idCine = funcion.idCine;
            //    venta.codigoTipo = item.codigoTipo;
            //    venta.detalle = item.detalle;
            //    venta.idFuncionDetalle = item.id;
            //    venta.fechaHora = funcion.fechaHora;
            //    venta.formato = funcion.formato;
            //    venta.idEmpresa = funcion.Cines.idEmpresa;
            //    venta.importe = item.precio;
            //    venta.lenguaje = funcion.lenguaje;
            //    venta.idPelicula = funcion.idPelicula;
            //    venta.sala = funcion.sala;
            //    venta.estado = "Reservada";
            //    venta.dondeCompro = "Web";

            //    //DTO
            //    venta.importeUnitarioString = item.precio.ToString("N2");
            //    venta.importeUnitario = item.precio * venta.cantidad;
            //    venta.importeString = venta.importe.ToString("N2");

            //    ventas.Add(venta);
            //}

            //gvEntradas.DataSource = ventas;
            //gvEntradas.DataBind();
            pnlEntrada.Visible = true;
            pnlPagar.Visible = false;
            update.Update();
        }

    }
}