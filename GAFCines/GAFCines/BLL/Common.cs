﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BLL
{
    public class Common
    {
        public static string DiaDeSemana(DateTime fecha)
        {
            string diaSemana = "";
            switch ((int)fecha.Date.DayOfWeek)
            {
                case 0:
                    diaSemana = "Domingo";
                    break;
                case 1:
                    diaSemana = "Lunes";
                    break;
                case 2:
                    diaSemana = "Martes";
                    break;
                case 3:
                    diaSemana = "Miércoles";
                    break;
                case 4:
                    diaSemana = "Jueves";
                    break;
                case 5:
                    diaSemana = "Viernes";
                    break;
                case 6:
                    diaSemana = "Sábado";
                    break;
                default:
                    diaSemana = "";
                    break;
            }
            return diaSemana;
        }

        public static string NombreDelMes(DateTime fecha)
        {
            string nombreMes = "";
            switch ((int)fecha.Date.Month)
            {
                case 1:
                    nombreMes = "Enero";
                    break;
                case 2:
                    nombreMes = "Febrero";
                    break;
                case 3:
                    nombreMes = "Marzo";
                    break;
                case 4:
                    nombreMes = "Abril";
                    break;
                case 5:
                    nombreMes = "Mayo";
                    break;
                case 6:
                    nombreMes = "Junio";
                    break;
                case 7:
                    nombreMes = "Julio";
                    break;
                case 8:
                    nombreMes = "Agosto";
                    break;
                case 9:
                    nombreMes = "Septiembre";
                    break;
                case 10:
                    nombreMes = "Octubre";
                    break;
                case 11:
                    nombreMes = "Noviembre";
                    break;
                case 12:
                    nombreMes = "Diciembre";
                    break;
                default:
                    nombreMes = "";
                    break;
            }
            return nombreMes;
        }
    }
}


