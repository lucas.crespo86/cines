﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Configuration;

namespace BLL
{
    public class Variables
    {

        public static DAL.Variables TraerUno(string variable)
        {
            using (DAL.CineEntities dc = new DAL.CineEntities())
            {
                return (from x in dc.Variables
                        where x.nombre == variable
                        select x).FirstOrDefault();
            }
        }
    }
}