﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using MercadoPago.Common;
using MercadoPago.DataStructures.Preference;
using MercadoPago.Resources;
using System.Collections;

namespace BLL
{
    public class IPN
    {
        public static void Create(MercadoPago.Resources.Payment payment, string Body)
        {
            if (payment.Status == PaymentStatus.approved)
            {
                string externalReference = payment.ExternalReference;
                string codigoVentaString = externalReference.Split(Convert.ToChar("_"))[1];
                int codigoVenta = Convert.ToInt32(codigoVentaString);
                string cineAbreviatura = externalReference.Split(Convert.ToChar("_"))[0];

                DAL.Cines cine = BLL.Cines.TraerUnoAbreviatura(cineAbreviatura);

                //string date_created = collection["date_created"].ToString();
                string email = payment.Payer.Email.ToString();
                string name = payment.Payer.FirstName + " " + payment.Payer.LastName;

                string dni = payment.Payer.Identification.Value.Number;
                
                if (payment.Card.Value.CardHolder != null)
                {
                    dni = payment.Card.Value.CardHolder.Value.Identification.Value.Number;
                }

                List<DAL.Ventas> ventas = BLL.Ventas.TraerTodos(codigoVenta, cine.id);

                if (ventas.Count > 0)
                {
                    if (payment.Status == PaymentStatus.approved)
                    {
                        foreach (DAL.Ventas item in ventas)
                        {
                            item.nroDocumento = dni;
                            item.email = email;
                            item.estado = "Pagada";
                            
                            BLL.Ventas.Editar(item);
                        }

                        
                        Body = Body.Replace("#NombreCliente#", name);
                        Body = Body.Replace("#CodigoRetiro#", codigoVenta.ToString());
                        Body = Body.Replace("#NombrePelicula#", ventas[0].nombrePelicula + " | " + ventas[0].lenguaje);
                        Body = Body.Replace("#Fecha#", ventas[0].fechaString);
                        Body = Body.Replace("#Hora#", ventas[0].horaString);
                        Body = Body.Replace("#NombreCine#", cine.nombre);
                        Body = Body.Replace("#Sala#", ventas[0].sala);
                        Body = Body.Replace("#PaginaCine#", cine.paginaWeb);
                        Body = Body.Replace("#DireccionCine#", cine.direccion);
                        Body = Body.Replace("#TituloCine#", cine.nombre);
                        Body = Body.Replace("#TextoMail#", cine.textoMail);

                        System.Net.Mail.MailMessage correo = new System.Net.Mail.MailMessage();
                        correo.From = new System.Net.Mail.MailAddress("query.noreply@gmail.com");
                        correo.To.Add(email);
                        correo.To.Add(cine.emailCopiaVenta);
                        correo.Subject = cine.nombre + " Codigo de Retiro";
                        correo.Body = Body;
                        correo.IsBodyHtml = true;
                        correo.Priority = System.Net.Mail.MailPriority.Normal;
                        System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                        smtp.Host = "smtp.gmail.com";
                        smtp.Port = 587;
                        smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                        smtp.UseDefaultCredentials = false;
                        smtp.EnableSsl = true;
                        smtp.Credentials = new System.Net.NetworkCredential("query.noreply@gmail.com", "123noreply");
                        smtp.Send(correo);
                    }

                    bool cancelar = false;
                    string estado = "";
                    if (payment.Status == PaymentStatus.cancelled)
                    {
                        estado = "Candelada";
                        cancelar = true;
                    }
                    if (payment.Status == PaymentStatus.in_process)
                    {
                        estado = "CanceladaInProceso";
                        cancelar = true;
                    }
                    if (payment.Status == PaymentStatus.authorized)
                    {
                        estado = "CanceladaAutorizada";
                        cancelar = true;
                    }
                    if (payment.Status == PaymentStatus.pending)
                    {
                        estado = "CanceladaPendiente";
                        cancelar = true;
                    }
                    if (payment.Status == PaymentStatus.rejected)
                    {
                        estado = "Cancelada";
                        cancelar = true;
                    }
                    if (cancelar == true)
                    {
                        try
                        {
                            payment.Refund();
                            foreach (DAL.Ventas item in ventas)
                            {
                                item.estado = estado;
                                BLL.Ventas.Editar(item);
                            }
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }

                }
            }
        }
    }
}