﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Configuration;

namespace BLL
{
    public class Cines
    {
        public static List<DAL.Cines> TraerTodos()
        {
            string cref = ConfigurationManager.AppSettings.Get("cref");

            using (DAL.CineEntities dc = new DAL.CineEntities())
            {
                DAL.Cines cine = (from x in dc.Cines
                                 where x.cref == cref
                                 select x).SingleOrDefault();

                List<DAL.Cines> cines = (from x in dc.Cines.Include(x => x.Empresas)
                                        where x.idEmpresa == cine.Empresas.id &&
                                        x.mostrar == true &&
                                        x.mantenimiento == false &&
                                        x.complejo == true
                                        select x).ToList();
                if (cines.Count == 0)
                {
                    cines.Add(cine);
                }
                return cines;
            }
        }

        public static DAL.Cines TraerUno(string cref)
        {
            using (DAL.CineEntities dc = new DAL.CineEntities())
            {
                return (from x in dc.Cines.Include(x => x.Empresas)
                        where x.cref == cref
                        select x).FirstOrDefault();
            }
        }

        public static DAL.Cines TraerUnoAbreviatura(string abreviatura)
        {
            using (DAL.CineEntities dc = new DAL.CineEntities())
            {
                return (from x in dc.Cines.Include(x => x.Empresas)
                        where x.abreviatura == abreviatura
                        select x).FirstOrDefault();

                
            }
        }
    }
}