﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Linq.Expressions;

namespace BLL
{
    public class Funciones
    {
        public static string TraerHorarios(DAL.Peliculas pelicula)
        {
            using (DAL.CineEntities dc = new DAL.CineEntities())
            {
                DateTime hoy = DateTime.Now;
                hoy = hoy.Date;
                switch (Common.DiaDeSemana(hoy))
                {
                    case "Lunes":
                        hoy = hoy.AddDays(-4);
                        break;
                    case "Martes":
                        hoy = hoy.AddDays(-5);
                        break;
                    case "Miércoles":
                        hoy = hoy.AddDays(-6);
                        break;
                    case "Jueves":
                        break;
                    case "Viernes":
                        hoy = hoy.AddDays(-1);
                        break;
                    case "Sábado":
                        hoy = hoy.AddDays(-2);
                        break;
                    case "Domingo":
                        hoy = hoy.AddDays(-3);
                        break;
                    default:
                        break;
                }

                string cref = dc.Cines.Where(x => x.id == pelicula.idCine).Select(x => x.cref).FirstOrDefault();
                var funciones = (from x in dc.Funciones
                                 where x.idCine == pelicula.idCine &&
                                    x.idPelicula == pelicula.id &&
                                    x.fechaHoraFicticia >= hoy && x.mostrar == true
                                 select x).ToList();
                    
                string tabla = "";
                DateTime recorrer = hoy.AddDays(7);
                List<DAL.Funciones> existen = funciones.Where(x => x.fechaHoraFicticia.Date >= hoy.Date && x.fechaHoraFicticia.Date < recorrer.Date).Select(x => x).ToList();


                if (existen.Count > 0)
                {
                    tabla += "<div class='table-responsive' style='overflow: auto; overflow-y: hidden; margin: 0 auto; white-space: nowrap'>";
                    tabla += "<table>";
                    tabla += "<tr style='background-color:#dddddd'>";
                    tabla += "<td style='background-color: #ffffff'></td>";

                    recorrer = hoy;
                    for (int i = 0; i < 7; i++)
                    {

                        tabla += "<td style='text-align:center'>";
                        tabla += "<b>" + Common.DiaDeSemana(recorrer) + "</b><br/>" + recorrer.Day + "/" + recorrer.Month;
                        tabla += "</td>";

                        recorrer = recorrer.AddDays(1);
                    }
                    tabla += "</tr>";

                    List<string> horasTrasnoche = existen.OrderBy(x => x.fechaHora.ToString("HH:mm")).Where(x => x.fechaHora.TimeOfDay <= TimeSpan.FromHours(5)).Select(x => x.fechaHoraFicticia.ToString("HH:mm") + " hs").Distinct().ToList();

                    List<string> horas = existen.OrderBy(x => x.fechaHora.ToString("HH:mm")).Where(x => x.fechaHora.TimeOfDay >= TimeSpan.FromHours(5)).Select(x => x.fechaHoraFicticia.ToString("HH:mm") + " hs").Distinct().ToList();

                    foreach (string item in horasTrasnoche)
                    {
                        horas.Add(item);
                    }

                    foreach (string item in horas)
                    {
                        tabla += "<tr style='height:45px'>";

                        tabla += "<td style='background-color: #dddddd;text-align: center'>";
                        tabla += item;
                        tabla += "</td>";

                        recorrer = hoy;

                        for (int i = 0; i < 7; i++)
                        {

                            tabla += "<td style='text-align:center'>";

                            List<DAL.Funciones> Filtradas = funciones.Where(x => x.fechaHoraFicticia.ToString("HH:mm") + " hs" == item && x.fechaHoraFicticia.Date == recorrer.Date).OrderBy(x => x.fechaHoraFicticia).ToList();
                            foreach (DAL.Funciones funcion in Filtradas)
                            {
                                tabla += "<p style='font-size:x-small;margin:0'><label style='background-color:#fe505a; color:#ffffff; padding:1px 4px'>" + funcion.lenguaje + " | " + "Sala " + funcion.sala + "</label></p>";
                            }

                            tabla += "</td>";

                            recorrer = recorrer.AddDays(1);
                        }


                        tabla += "</tr>";
                    }

                    tabla += "</table>";
                    tabla += "</div>";
                    tabla += "<div class='movie__btns'><a href='pelicula.aspx?pref=" + pelicula.pref + "&cref=" + cref + "' class='btn btn-md btn--warning'><i class='fa fa-shopping-cart'></i>  COMPRAR ENTRADAS</a></div>";

                    return tabla;
                }
                else
                {
                    tabla = "<div>";
                    tabla += "Venta de entradas anticipadas DISPONIBLES en PREVENTA";
                    tabla += "</div>";
                    tabla += "<div class='movie__btns'><a href='pelicula.aspx?pref=" + pelicula.pref + "&cref=" + cref + "' class='btn btn-md btn--warning'><i class='fa fa-shopping-cart'></i>  PREVENTA</a></div>";
                    return tabla;
                }

            }
        }

        public static List<DAL.Funciones> TraerTodos(DAL.Peliculas pelicula)
        {
            using (DAL.CineEntities dc = new DAL.CineEntities())
            {
                var cine = dc.Cines.Where(x => x.id == pelicula.idCine).FirstOrDefault();

                DateTime hoy = DateTime.Now.AddMinutes(cine.minutosHabilitados);
          
                var funciones = dc.Funciones.Include(x => x.Cines)
                    .Where(x => x.idCine == pelicula.idCine &&
                            x.Peliculas.id == pelicula.id &&
                            x.fechaHora >= hoy && x.mostrar == true && x.Cines.mostrar==true && x.Peliculas.mostrar==true)
                            .OrderBy(x => x.fechaHora)
                        .ToList();

                return funciones;

            }
        }

        public static DAL.Funciones TraerUno(string fref, string cref)
        {
            using (DAL.CineEntities dc = new DAL.CineEntities())
            {
                return (from x in dc.Funciones.Include(x => x.FuncionesDetalle).Include(x=> x.Peliculas).Include(x => x.Cines).Include(x => x.Cines.Empresas)
                        where x.fref == fref &&
                              x.Cines.cref == cref &&
                              x.mostrar == true && x.Peliculas.vender == true
                           select x).FirstOrDefault();
            }
        }

        public static void CambiarEstado(int id, bool estado)
        {
            using (DAL.CineEntities dc = new DAL.CineEntities())
            {
                dc.Funciones.Where(x => x.id == id).FirstOrDefault().vender = estado;
                dc.SaveChanges();
            }
        }

        public static int TraerCantidadVendidas(DAL.Funciones funcion)
        {
            using (DAL.CineEntities dc = new DAL.CineEntities())
            {
                int cantidad = 0;
                List<DAL.FuncionesDetalle> funcionesDetalle = (from x in dc.FuncionesDetalle
                                                               where x.Funciones.id == funcion.id
                                                               select x).ToList();

                foreach (DAL.FuncionesDetalle item in funcionesDetalle)
                {
                    List<DAL.Ventas> ventas = (from x in dc.Ventas
                                               where x.idFuncionDetalle == item.id
                                               && (x.estado.Trim() == "Pagada" || x.estado.Trim() == "Descargada" || x.estado.Trim() == "Entregada")
                                               select x).ToList();
                    foreach (DAL.Ventas venta in ventas)
                    {
                        cantidad = cantidad + venta.cantidad;
                    }
                }

                return cantidad;

            }
        }
    }
}