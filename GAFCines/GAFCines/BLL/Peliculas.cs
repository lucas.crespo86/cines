﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Configuration;

namespace BLL
{
    public class Peliculas
    {
        public static List<DAL.Peliculas> TraerTodosConFuncion(string cref)
        {
            using (DAL.CineEntities dc = new DAL.CineEntities())
            {
                return dc.Funciones.Include(x => x.Peliculas).Where(x => x.fechaHoraFicticia >= DateTime.Now &&
                               x.Cines.cref == cref &&
                               x.mostrar == true)
                               .Select(x => x.Peliculas).Where(x => x.Cines.cref == cref).Distinct().ToList();

               
            }
        }

        public static DAL.Peliculas TraerUno(string pref,string cref)
        {
            using (DAL.CineEntities dc = new DAL.CineEntities())
            {
                return (from x in dc.Peliculas.Include(x => x.Cines)
                        where x.Cines.cref == cref &&
                              x.pref == pref &&
                               x.vender == true &&
                               x.mostrar == true
                        select x).FirstOrDefault();

            }
        }
    }
}