﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Linq.Expressions;

namespace BLL
{
    [Serializable]
    public class VentasDTO
    {
        public int id { get; set; }
        public int cantidad { get; set; }
        public decimal importe { get; set; }
        public System.DateTime fechaVenta { get; set; }
        public int codigoVenta { get; set; }
        public string detalle { get; set; }
        public int codigoTipo { get; set; }
        public string email { get; set; }
        public string nroDocumento { get; set; }
        public int idPelicula { get; set; }
        public int idCine { get; set; }
        public int idEmpresa { get; set; }
        public int idFuncionDetalle { get; set; }
        public string nombrePelicula { get; set; }
        public System.DateTime fechaHora { get; set; }
        public string fechaString { get; set; }
        public string horaString { get; set; }
        public string lenguaje { get; set; }
        public string formato { get; set; }
        public string sala { get; set; }
        public int codigoFuncionDetalle { get; set; }
        public string dondeCompro { get; set; }
        public string dondeRetiro { get; set; }
        public string estado { get; set; }
        public string nroEntrada { get; set; }
        public int posicion { get; set; }

        //DTO
        public string importeString { get; set; }
        public decimal importeUnitario { get; set; }
        public string importeUnitarioString { get; set; }
    }

    public class Ventas
    {
        public static DAL.Ventas Create(BLL.VentasDTO ventaDTO)
        {
            using (DAL.CineEntities dc = new DAL.CineEntities())
            {
                DAL.Ventas venta = new DAL.Ventas
                {
                    cantidad = ventaDTO.cantidad,
                    importe = ventaDTO.importe,
                    fechaVenta = ventaDTO.fechaVenta,
                    codigoVenta = ventaDTO.codigoVenta,
                    detalle = ventaDTO.detalle,
                    codigoTipo = ventaDTO.codigoTipo,
                    idPelicula = ventaDTO.idPelicula,
                    idCine = ventaDTO.idCine,
                    idEmpresa = ventaDTO.idEmpresa,
                    idFuncionDetalle = ventaDTO.idFuncionDetalle,
                    nombrePelicula = ventaDTO.nombrePelicula,
                    fechaHora = ventaDTO.fechaHora,
                    fechaString = ventaDTO.fechaString,
                    horaString = ventaDTO.horaString,
                    lenguaje = ventaDTO.lenguaje,
                    formato = ventaDTO.formato,
                    sala = ventaDTO.sala,
                    codigoFuncionDetalle = ventaDTO.codigoFuncionDetalle,
                    dondeCompro = ventaDTO.dondeCompro,
                    dondeRetiro = ventaDTO.dondeRetiro,
                    estado = ventaDTO.estado,
                };
                dc.Ventas.Add(venta);
                dc.SaveChanges();

                return venta;
            }
        }

        public static List<DAL.Ventas> TraerTodos(int codigoVenta, int idCIne)
        {
            using (DAL.CineEntities dc = new DAL.CineEntities())
            {
                return (from x in dc.Ventas
                        where x.codigoVenta == codigoVenta &&
                        x.idCine == idCIne
                        select x).ToList();

            }
        }

        public static DAL.Ventas Editar(DAL.Ventas venta)
        {
            using (DAL.CineEntities dc = new DAL.CineEntities())
            {
                DAL.Ventas ventaPersistido = (from x in dc.Ventas
                                              where x.id == venta.id
                                              select x).FirstOrDefault();

                if (ventaPersistido != null)
                {
                    ventaPersistido.estado = venta.estado;
                    ventaPersistido.nroDocumento = venta.nroDocumento;
                    ventaPersistido.nroEntrada = venta.nroEntrada;
                    ventaPersistido.email = venta.email;

                    dc.SaveChanges();
                }
            }
            return venta;
        }
    }
}