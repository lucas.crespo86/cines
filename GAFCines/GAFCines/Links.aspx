﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Links.aspx.cs" Inherits="GAFCines.Links" %>

<!doctype html>
<html>
<head>
    <!-- Basic Page Needs -->
     <% string cref = Request.QueryString["cref"];
                DAL.Cines cine = BLL.Cines.TraerUno(cref); %>
    <meta charset="utf-8">
    <title><%= cine.nombre %></title>

    <!-- Mobile Specific Metas-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Custom -->
    <link href="assets/css/style.css?v=1" rel="stylesheet" />

</head>

<body>
    <form id="form1" method="post" runat="server">
        <div class="wrapper">
            <!-- Banner -->
            <% string cref = Request.QueryString["cref"];
                DAL.Cines cine = BLL.Cines.TraerUno(cref); %>
            <div class="banner-top">
                <img src="assets/images/banners/bra.jpg" alt="banner publicidad">
            </div>

            <!-- Header section -->
            <header class="header-wrapper header-wrapper--home">
                <div class="container">
                    <!-- Logo link-->
                    <a href="<%= cine.paginaWeb %>" class="logo" style="color: #ffffff; font-size: 20px;"><%= cine.nombre %></a>
                </div>
            </header>
            
            <section class="container" style="min-height:500px">
            <div class="col-sm-12">
                <h2 class="page-heading">PELÍCULAS EN CARTELERA</h2>

                <div class="select-area" style="height: 60px">
                    <span class="datepicker__marker"><%= cine.nombre %></span>
                </div>
                <%foreach (DAL.Peliculas pelicula in BLL.Peliculas.TraerTodosConFuncion(cine.cref))
                    {
                    %>
                    <div class="movie movie--preview movie--full release">

                        <div class="col-sm-12 col-md-12 col-lg-12 movie__about">

                            <label class="movie__title link--huge""><%= pelicula.nombre%></label>
                            <hr>
                            <%= BLL.Funciones.TraerHorarios(pelicula) %>
                        </div>
                    </div>
                    <hr />
                    <div class="clearfix"></div>
                    <%} %>
            </div>
            <!-- end movie preview item -->

            </section>
            <section class="container">
                <div class="clearfix"></div>
            </section>
            <footer class="footer-wrapper">
                <section class="container">
                    <div class="col-xs-12 col-md-12">
                        <div class="footer-info">
                            <p class="heading-special--small">
                                CINE<br>
                                <span class="title-edition">&copy; <%= DateTime.Now.Year %>. Todos los derechos reservador</span>
                            </p>
                            <div class="clearfix"></div>
                            <p class="copy">Powered by GAFCines</p>
                        </div>
                    </div>
                </section>
            </footer>
        </div>
    </form>

</body>
</html>
