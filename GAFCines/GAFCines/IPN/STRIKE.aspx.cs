﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using MercadoPago.Common;
using MercadoPago.DataStructures.Preference;
using MercadoPago.Resources;
using System.Collections;


namespace GAFCines.IPN
{
    public partial class STRIKE : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                MercadoPago.SDK.CleanConfiguration();
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                DAL.Cines cine = BLL.Cines.TraerUno("6010241");
                
                MercadoPago.SDK.ClientId = cine.Empresas.mpClientId;
                MercadoPago.SDK.ClientSecret = cine.Empresas.mpClientSecret;

                long id = Convert.ToInt64(Request.QueryString["id"]);

                var payment = MercadoPago.Resources.Payment.FindById(id);
                string Body = System.IO.File.ReadAllText(Server.MapPath("../assets/EmailTemplate.html"));

                BLL.IPN.Create(payment, Body);
                MercadoPago.SDK.CleanConfiguration();
            }
            catch (Exception)
            {

                throw;
            }
            
        }
    }
}