﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Collections;

namespace Impresora
{
    public class CBOC
    {
        ArrayList headerLines = new ArrayList();
        ArrayList subHeaderLines = new ArrayList();
        ArrayList items = new ArrayList();
        ArrayList totales = new ArrayList();
        ArrayList footerLines = new ArrayList();
        private Image headerImage = null;

        int count = 0;

        int maxChar = 35;
        int maxCharDescription = 20;

        int imageHeight = 0;

        float leftMargin = 0;
        float topMargin = 3;

        string fontName = "FontB11";
        int fontSize = 6;

        string fontName1 = "FontB11";
        int fontSize1 = 8;

        string fontName2 = "FontB11";
        int fontSize2 = 12;

        string fontName3 = "FontB11";
        int fontSize3 = 14;

        string fontName4 = "FontB11";
        int fontSize4 = 16;

        Font printFont = null;
        Font printFont1 = null;
        Font printFont2 = null;
        Font printFont3 = null;
        Font printFont4 = null;
        SolidBrush myBrush = new SolidBrush(Color.Black);

        Graphics gfx = null;

        string line = null;

        float quePosicion = 0;
        float altoRenglon = 0;

        public CBOC()
        {

        }

        public Image HeaderImage
        {
            get { return headerImage; }
            set { if (headerImage != value) headerImage = value; }
        }

        public int MaxChar
        {
            get { return maxChar; }
            set { if (value != maxChar) maxChar = value; }
        }

        public int MaxCharDescription
        {
            get { return maxCharDescription; }
            set { if (value != maxCharDescription) maxCharDescription = value; }
        }

        public int FontSize
        {
            get { return fontSize; }
            set { if (value != fontSize) fontSize = value; }
        }

        public string FontName
        {
            get { return fontName; }
            set { if (value != fontName) fontName = value; }
        }

        public void AddHeaderLine(string line)
        {
            headerLines.Add(line);
        }

        public void AddSubHeaderLine(string line)
        {
            subHeaderLines.Add(line);
        }

        public void AddItem(string Col1, string Col2, string queFuente)
        {
            OrderItem newItem = new OrderItem('?');
            items.Add(newItem.GenerateItem(Col1, Col2, queFuente));
        }

        public void AddTotal(string name, string price)
        {
            OrderTotal newTotal = new OrderTotal('?');
            totales.Add(newTotal.GenerateTotal(name, price));
        }

        public void AddFooterLine(string line)
        {
            footerLines.Add(line);
        }

        private string AlignRightText(int lenght)
        {
            string espacios = "";
            int spaces = maxChar - lenght;
            for (int x = 0; x < spaces; x++)
                espacios += " ";
            return espacios;
        }

        private string DottedLine()
        {
            string dotted = "";
            for (int x = 0; x < maxChar; x++)
                dotted += "=";
            return dotted;
        }

        public bool PrinterExists(string impresora)
        {
            foreach (String strPrinter in PrinterSettings.InstalledPrinters)
            {
                if (impresora == strPrinter)
                    return true;
            }
            return false;
        }

        public void PrintTicket(string impresora)
        {
            printFont = new Font(fontName, fontSize, FontStyle.Regular);
            printFont1 = new Font(fontName1, fontSize1, FontStyle.Regular);
            printFont2 = new Font(fontName2, fontSize2, FontStyle.Regular);
            printFont3 = new Font(fontName3, fontSize3, FontStyle.Regular);
            printFont4 = new Font(fontName4, fontSize4, FontStyle.Regular);

            PrintDocument pr = new PrintDocument();
            pr.PrinterSettings.PrinterName = impresora;
            pr.PrintPage += new PrintPageEventHandler(pr_PrintPage);
            pr.Print();
        }

        private void pr_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            gfx = e.Graphics;

            DrawImage();
            DrawHeader();
            DrawSubHeader();
            DrawItems();
            DrawTotales();
            DrawFooter();

            if (headerImage != null)
            {
                HeaderImage.Dispose();
                headerImage.Dispose();
            }
        }

        private float YPosition(int queFuente)
        {

            /*
            ***************** ACA DEBERIA SUMAR EL ALTO DEL FONT DEL RENGLON o bien elegir el mas alto de todos **********************
            */
            switch (queFuente)
            {
                case 1:
                    return topMargin + (count * printFont1.GetHeight(gfx) + imageHeight);
                    break;
                case 2:
                    return topMargin + (count * printFont2.GetHeight(gfx) + imageHeight);
                    break;
                case 3:
                    return topMargin + (count * printFont3.GetHeight(gfx) + imageHeight);
                    break;
                default:
                    return topMargin + (count * printFont4.GetHeight(gfx) + imageHeight);
                    break;
            }

        }

        private float SumaPosicion(int queFuente)
        {

            /*
            ***************** ACA DEBERIA SUMAR EL ALTO DEL FONT DEL RENGLON o bien elegir el mas alto de todos **********************
            */

            switch (queFuente)
            {
                case 1:
                    return quePosicion += printFont1.GetHeight(gfx) + printFont1.GetHeight(gfx);
                    break;
                case 2:
                    return quePosicion += printFont2.GetHeight(gfx) + printFont1.GetHeight(gfx);
                    break;
                case 3:
                    return quePosicion += printFont3.GetHeight(gfx) + printFont1.GetHeight(gfx);
                    break;
                default:
                    return quePosicion += printFont4.GetHeight(gfx) + printFont1.GetHeight(gfx);
                    break;
            }

        }

        private void DrawImage()
        {
            if (headerImage != null)
            {
                try
                {
                    gfx.DrawImage(headerImage, new Point((int)leftMargin, 30)); // (int)YPosition(1)));
                    double height = ((double)headerImage.Height / 58) * 15;
                    imageHeight = (int)Math.Round(height) + 3;
                }
                catch (Exception)
                {
                }
            }
        }

        private void DrawHeader()
        {
            foreach (string header in headerLines)
            {
                if (header.Length > maxChar)
                {
                    int currentChar = 0;
                    int headerLenght = header.Length;

                    while (headerLenght > maxChar)
                    {
                        line = header.Substring(currentChar, maxChar);
                        gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(1), new StringFormat());

                        count++;
                        currentChar += maxChar;
                        headerLenght -= maxChar;
                    }
                    line = header;
                    gfx.DrawString(line.Substring(currentChar, line.Length - currentChar), printFont, myBrush, leftMargin, YPosition(1), new StringFormat());
                    count++;
                }
                else
                {
                    line = header;
                    gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(1), new StringFormat());

                    count++;
                }
            }
            DrawEspacio();
        }

        private void DrawSubHeader()
        {
            foreach (string subHeader in subHeaderLines)
            {
                if (subHeader.Length > maxChar)
                {
                    int currentChar = 0;
                    int subHeaderLenght = subHeader.Length;

                    while (subHeaderLenght > maxChar)
                    {
                        line = subHeader;
                        gfx.DrawString(line.Substring(currentChar, maxChar), printFont, myBrush, leftMargin, YPosition(1), new StringFormat());

                        count++;
                        currentChar += maxChar;
                        subHeaderLenght -= maxChar;
                    }
                    line = subHeader;
                    gfx.DrawString(line.Substring(currentChar, line.Length - currentChar), printFont, myBrush, leftMargin, YPosition(1), new StringFormat());
                    count++;
                }
                else
                {
                    line = subHeader;

                    gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(1), new StringFormat());

                    count++;

                    line = DottedLine();

                    gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(1), new StringFormat());

                    count++;
                }
            }
            DrawEspacio();
        }

        private void DrawItems()
        {
            OrderItem ordIt = new OrderItem('?');


            altoRenglon = 80 / (items.Count + 1);

            foreach (string item in items)
            {
                line = ordIt.GetqueFuente(item);

                quePosicion += altoRenglon;

                leftMargin = 47;
                line = ordIt.GetItemCol1(item);

                gfx.DrawString(line, printFont, myBrush, leftMargin, quePosicion, new StringFormat());

                line = ordIt.GetqueFuente(item);

                switch (Convert.ToInt16(line))
                {
                    case 1:
                        leftMargin = 0;
                        line = ordIt.GetItemCol1(item);
                        gfx.DrawString(line, printFont1, myBrush, leftMargin, quePosicion, new StringFormat());
                        break;
                    case 2:
                        leftMargin = 0;
                        line = ordIt.GetItemCol1(item);
                        gfx.DrawString(line, printFont2, myBrush, leftMargin, quePosicion, new StringFormat());
                        break;
                    case 3:
                        leftMargin = 0;
                        line = ordIt.GetItemCol1(item);
                        gfx.DrawString(line, printFont3, myBrush, leftMargin, quePosicion, new StringFormat());
                        break;
                    default:
                        leftMargin = 0;
                        line = ordIt.GetItemCol1(item);
                        gfx.DrawString(line, printFont4, myBrush, leftMargin, quePosicion, new StringFormat());
                        break;
                }

                count++;

            }

            leftMargin = 0;
            DrawEspacio();

            leftMargin = 0;
            gfx.DrawString("Talon del espectador", printFont, myBrush, leftMargin, 80, new StringFormat());
            leftMargin = 50;
            gfx.DrawString("Talon de URNA", printFont, myBrush, leftMargin, 80, new StringFormat());

            count++;
            DrawEspacio();
        }

        private void DrawTotales()
        {
            OrderTotal ordTot = new OrderTotal('?');

            foreach (string total in totales)
            {
                line = ordTot.GetTotalCantidad(total);
                line = AlignRightText(line.Length) + line;

                gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(1), new StringFormat());
                leftMargin = 0;

                line = "      " + ordTot.GetTotalName(total);
                gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(1), new StringFormat());
                count++;
            }
            leftMargin = 0;
            DrawEspacio();
            DrawEspacio();
        }

        private void DrawFooter()
        {
            foreach (string footer in footerLines)
            {
                if (footer.Length > maxChar)
                {
                    int currentChar = 0;
                    int footerLenght = footer.Length;

                    while (footerLenght > maxChar)
                    {
                        line = footer;
                        gfx.DrawString(line.Substring(currentChar, maxChar), printFont, myBrush, leftMargin, YPosition(1), new StringFormat());

                        count++;
                        currentChar += maxChar;
                        footerLenght -= maxChar;
                    }
                    line = footer;
                    gfx.DrawString(line.Substring(currentChar, line.Length - currentChar), printFont, myBrush, leftMargin, YPosition(1), new StringFormat());
                    count++;
                }
                else
                {
                    line = footer;
                    gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(1), new StringFormat());

                    count++;
                }
            }
            leftMargin = 0;
            DrawEspacio();
        }

        private void DrawEspacio()
        {
            line = "";

            gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(1), new StringFormat());

            count++;
        }
    }

    public class OrderItem
    {
        char[] delimitador = new char[] { '?' };

        public OrderItem(char delimit)
        {
            delimitador = new char[] { delimit };
        }

        public string GetItemCol1(string orderItem)
        {
            string[] delimitado = orderItem.Split(delimitador);
            return delimitado[0];
        }

        public string GetItemCol2(string orderItem)
        {
            string[] delimitado = orderItem.Split(delimitador);
            return delimitado[1];
        }

        public string GetqueFuente(string orderItem)
        {
            string[] delimitado = orderItem.Split(delimitador);
            return delimitado[2];
        }



        public string GenerateItem(string col1, string col2, string queFuente)
        {
            return col1 + delimitador[0] + col2 + delimitador[0] + queFuente;
        }
    }

    public class OrderTotal
    {
        char[] delimitador = new char[] { '?' };

        public OrderTotal(char delimit)
        {
            delimitador = new char[] { delimit };
        }

        public string GetTotalName(string totalItem)
        {
            string[] delimitado = totalItem.Split(delimitador);
            return delimitado[0];
        }

        public string GetTotalCantidad(string totalItem)
        {
            string[] delimitado = totalItem.Split(delimitador);
            return delimitado[1];
        }

        public string GenerateTotal(string totalName, string price)
        {
            return totalName + delimitador[0] + price;
        }
    }
}