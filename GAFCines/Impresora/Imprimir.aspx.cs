﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;

namespace Impresora
{
    public partial class Imprimir : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int codigoRetiro = Convert.ToInt32(Request.QueryString["codigo"]);
                int documento = Convert.ToInt32(Request.QueryString["documento"]);
                int idTerminal = Convert.ToInt32(Request.QueryString["idTerminal"]);

                using (var ctx = new CineEntities())
                {
                    var terminal = ctx.Terminales.Where(x => x.id == idTerminal)
                                            .SingleOrDefault();



                    List<Ventas> ventas = ctx.Ventas.Where(x => x.codigoVenta == codigoRetiro && x.idCine == terminal.idCine
                                                            && x.estado == "Descargada").ToList();

                    string fechaHoraActual = DateTime.Now.ToString("dd/MM/yyyy") + " - " + DateTime.Now.ToString("HH:mm");

                    if (ventas.Count > 0)
                    {
                        foreach (Ventas item in ventas)
                        {
                            if (item.nroDocumento == null || item.nroDocumento == documento.ToString())
                            {
                                item.nroEntrada = ctx.FuncionesDetalle.Where(x => x.id == item.idFuncionDetalle).SingleOrDefault().nroEntrada;

                                var pelicula = ctx.Peliculas.Where(x => x.id == item.idPelicula).SingleOrDefault();

                                var cine = ctx.Cines.Where(x => x.id == item.idCine).SingleOrDefault();

                                var posicionInicio = item.posicion;

                                var precio = item.importe / item.cantidad;

                                for (int i = 0; i < item.cantidad; i++)
                                {
                                    CBOC ticket = new CBOC();

                                    //string url = Server.MapPath("~/Images/marcaAgua.png");
                                    //url = url.Replace("\\", "/");

                                    //ticket.HeaderImage = System.Drawing.Image.FromFile(url);  //"~/Images/demo.png"; //esta propiedad no es obligatoria

                                    string posicion = item.posicion.ToString();
                                    while (posicion.ToString().Length < 4)
                                    {
                                        posicion = "0" + posicion;
                                    }
                                    ticket.AddItem(fechaHoraActual + "Hs", "", "1");
                                    ticket.AddItem(cine.nombre, "", "2");
                                    ticket.AddItem(pelicula.abreviatura, "", "3");
                                    ticket.AddItem("Fecha: " + item.fechaHora.ToString("dd/MM/yyyy"), "", "3");
                                    ticket.AddItem("Hora: " + item.horaString + "Hs", "", "3");
                                    ticket.AddItem("" + item.nroEntrada + posicion, "", "1");
                                    ticket.AddItem("Precio: $" + precio.ToString("N2"), "", "4");
                                    ticket.AddItem("Sala: " + item.sala, "", "1");
                                    ticket.AddItem(cine.CUIT, "", "1");

                                    ticket.PrintTicket("MSPDF");

                                    item.posicion = item.posicion + 1;
                                }

                                item.posicion = posicionInicio;
                                if (terminal.mantenimiento == false)
                                {
                                    item.estado = "Kiosco";
                                    item.dondeRetiro = "Kiosco";
                                    ctx.Entry(item).State = EntityState.Modified;
                                    ctx.SaveChanges();
                                }
                                Response.Redirect(terminal.urlKiosko + "?idTerminal=" + idTerminal);
                            }
                            
                        }
                    }
                    else
                    {
                        Response.Redirect("https://itag.com.ar/Kiosco");
                    }
                }
               
            }
            catch (Exception)
            {
                Response.Redirect("https://itag.com.ar/Kiosco/Home/Error");
            }
        }
    }
}