﻿using Kiosko.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Helper;
using DAL;

namespace Kiosko.Controllers
{
    [NoLogin]
    public class LoginController : Controller
    {
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Acceder(string codTerminal, string pass)
        {
            var rm = new ResponseModel();

            try
            {
                using (var ctx = new CineEntities())
                {
                    //Password = HashHelper.MD5(Password);

                    var terminal = ctx.Terminales.Where(x => x.codigoTerminal == codTerminal)
                                             .Where(x => x.pass == pass)
                                             .SingleOrDefault();

                    if (terminal != null)
                    {
                        SessionHelper.AddUserToSession(terminal.id.ToString());
                        rm.SetResponse(true);
                    }
                    else
                    {
                        rm.SetResponse(false, "Datos de sesión incorrectos");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            if (rm.response)
            {
                rm.href = Url.Content("~/");
            }

            return Json(rm);
        }

        [Autenticado]
        public ActionResult Logout()
        {
            SessionHelper.DestroyUserSession();
            return Redirect("~/");
        }
    }
}