﻿using Kiosko.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Helper;
using DAL;

namespace Kiosko.Controllers
{
    [Autenticado]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var terminal = new DAL.Terminales();
            try
            {
                using (var ctx = new CineEntities())
                {
                    //Password = HashHelper.MD5(Password);
                    var idTerminal = SessionHelper.GetUser();
                    terminal = ctx.Terminales.Where(x => x.id == idTerminal)
                                            .SingleOrDefault();

                    ViewBag.NombreCine = terminal.Cines.nombre;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return View(terminal);
        }

        public ActionResult Mensaje()
        {
            var terminal = new DAL.Terminales();
            try
            {
                using (var ctx = new CineEntities())
                {
                    //Password = HashHelper.MD5(Password);
                    var idTerminal = SessionHelper.GetUser();
                    terminal = ctx.Terminales.Where(x => x.id == idTerminal)
                                            .SingleOrDefault();

                    ViewBag.NombreCine = terminal.Cines.nombre;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return View(terminal);
        }

        public ActionResult Error()
        {
            var terminal = new DAL.Terminales();
            try
            {
                using (var ctx = new CineEntities())
                {
                    //Password = HashHelper.MD5(Password);
                    var idTerminal = SessionHelper.GetUser();
                    terminal = ctx.Terminales.Where(x => x.id == idTerminal)
                                            .SingleOrDefault();

                    ViewBag.NombreCine = terminal.Cines.nombre;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return View(terminal);
        }

        public ActionResult Entregar()
        {
            var terminal = new DAL.Terminales();
            try
            {
                using (var ctx = new CineEntities())
                {
                    //Password = HashHelper.MD5(Password);
                    var idTerminal = SessionHelper.GetUser();
                    terminal = ctx.Terminales.Where(x => x.id == idTerminal)
                                            .SingleOrDefault();

                    ViewBag.NombreCine = terminal.Cines.nombre;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return View();
        }

        [HttpPost]
        public JsonResult Entregar(int codigo, string documento)
        {
            var rm = new ResponseModel();
            rm.message = "No se pudieron encontrar entradas con los datos ingresados";
            try
            {
                using (var ctx = new CineEntities())
                {
                    var idTerminal = SessionHelper.GetUser();

                    var terminal = ctx.Terminales.Where(x => x.id == idTerminal)
                                             .SingleOrDefault();

                    if (terminal != null)
                    {
                        List<Ventas> ventas = ctx.Ventas.Where(x => x.codigoVenta == codigo && x.idCine == terminal.idCine &&
                                                                x.estado == "Descargada").ToList();

                        var valido = false;
                        foreach (var item in ventas)
                        {
                            if (item.nroDocumento == documento || item.nroDocumento == null)
                            {
                                valido = true;
                            }
                        }
                        if (valido == true)
                        {
                            rm.SetResponse(true);
                            rm.href = Url.Content(terminal.urlImpresion + "?codigo=" + codigo + "&documento=" + documento + "&idTerminal=" + terminal.id);
                        }
                        
                    }
                    else
                    {
                        rm.SetResponse(false, "Ocurrió un problema de sesión");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Json(rm);
        }


        public ActionResult Comprar()
        {
            var terminal = new DAL.Terminales();
            try
            {
                ViewBag.Pasos = "paso-1";
            }
            catch (Exception)
            {
                throw;
            }
            return View();
        }

        [HttpPost]
        public JsonResult TraerCines()
        {
            var rm = new ResponseModel();
            rm.message = "No se pudieron encontrar cines disponibles para vender";
            try
            {
                using (var ctx = new CineEntities())
                {
                    var idTerminal = SessionHelper.GetUser();

                    var terminal = ctx.Terminales.Where(x => x.id == idTerminal)
                                             .SingleOrDefault();

                    if (terminal != null)
                    {
                        List<Cines> cines = ctx.Cines.Where(x => x.idEmpresa == terminal.Cines.idEmpresa && x.complejo == true).ToList();
                        if (cines.Count == 1)
                        {
                            ViewBag.Pasos = "paso-2";
                        }

                        var valido = false;
                        foreach (var item in ventas)
                        {
                            if (item.nroDocumento == documento || item.nroDocumento == null)
                            {
                                valido = true;
                            }
                        }
                        if (valido == true)
                        {
                            rm.SetResponse(true);
                            rm.
                            rm.href = Url.Content(terminal.urlImpresion + "?codigo=" + codigo + "&documento=" + documento + "&idTerminal=" + terminal.id);
                        }

                    }
                    else
                    {
                        rm.SetResponse(false, "Ocurrió un problema de sesión");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Json(rm);
        }


    }
}